# translation of libkholidays.po to greek
#
# Spiros Georgaras <sng@hellug.gr>, 2005.
# Loukas Stamellos <lstamellos@power-on.gr>, 2005.
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2007, 2009, 2012.
# Dimitrios Glentadakis <dglent@gmail.com>, 2013.
# Stelios <sstavra@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libkholidays\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-06-13 09:34+0000\n"
"PO-Revision-Date: 2021-06-23 09:48+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Qt-Contexts: true\n"

#: astroseasons.cpp:139
msgctxt "AstroSeasons|"
msgid "June Solstice"
msgstr "Ηλιοστάσιο Ιουνίου"

#: astroseasons.cpp:141
msgctxt "AstroSeasons|"
msgid "December Solstice"
msgstr "Ηλιοστάσιο Δεκεμβρίου"

#: astroseasons.cpp:143
msgctxt "AstroSeasons|"
msgid "March Equinox"
msgstr "Ισημερία Μαρτίου"

#: astroseasons.cpp:145
msgctxt "AstroSeasons|"
msgid "September Equinox"
msgstr "Ισημερία Σεπτεμβρίου"

#: declarative/holidayregionsmodel.cpp:89
#, fuzzy
#| msgctxt "Header for Holiday Region column"
#| msgid "Region"
msgctxt "HolidayRegionsDeclarativeModel|"
msgid "Region"
msgstr "Περιοχή"

#: declarative/holidayregionsmodel.cpp:91
#, fuzzy
#| msgctxt "HolidayRegion|Holiday type"
#| msgid "Name Days"
msgctxt "HolidayRegionsDeclarativeModel|"
msgid "Name"
msgstr "Ονομαστική"

#: declarative/holidayregionsmodel.cpp:93
#, fuzzy
#| msgctxt "Header for Description column"
#| msgid "Description"
msgctxt "HolidayRegionsDeclarativeModel|"
msgid "Description"
msgstr "Περιγραφή"

#: holidayregion.cpp:886
msgctxt "HolidayRegion|Canadian region"
msgid "Quebec"
msgstr "Κεμπέκ"

#: holidayregion.cpp:889
msgctxt "HolidayRegion|German region"
msgid "Germany, Bavaria"
msgstr "Γερμανία, Βαυαρία"

#: holidayregion.cpp:891
msgctxt "HolidayRegion|German region"
msgid "Germany, Brandenburg"
msgstr "Γερμανία, Βρανδεμβούργο"

#: holidayregion.cpp:893
msgctxt "HolidayRegion|German region"
msgid "Germany, Berlin"
msgstr "Γερμανία, Βερολίνο"

#: holidayregion.cpp:895
msgctxt "HolidayRegion|German region"
msgid "Germany, Baden-Wuerttemberg"
msgstr "Γερμανία, Βάση-Βυρτεμβέργη"

#: holidayregion.cpp:897
msgctxt "HolidayRegion|German region"
msgid "Germany, Hesse"
msgstr "Γερμαία, Έσση"

#: holidayregion.cpp:899
msgctxt "HolidayRegion|German region"
msgid "Germany, Mecklenburg-Hither Pomerania"
msgstr "Γερμανία, Μέκλενμπουργκ-Χίδερ Πομερανία"

#: holidayregion.cpp:901
msgctxt "HolidayRegion|German region"
msgid "Germany, Lower Saxony"
msgstr "Φερμανία, Κάτω Σαξωνία"

#: holidayregion.cpp:903
msgctxt "HolidayRegion|German region"
msgid "Germany, North Rhine-Westphalia"
msgstr "Γερμανία Βόρειος Ρήνος-Βεστφαλία"

#: holidayregion.cpp:905
msgctxt "HolidayRegion|German region"
msgid "Germany, Rhineland-Palatinate"
msgstr "Γερμανία, Ρήνος-Παλατινάτο"

#: holidayregion.cpp:907
msgctxt "HolidayRegion|German region"
msgid "Germany, Schleswig-Holstein"
msgstr "Γερμανία, Σλέσβιχ-Χολστάιν"

#: holidayregion.cpp:909
msgctxt "HolidayRegion|German region"
msgid "Germany, Saarland"
msgstr "Γερμανία, Σάαρ"

#: holidayregion.cpp:911
msgctxt "HolidayRegion|German region"
msgid "Germany, Saxony"
msgstr "Γερμανία, Σαξωνία"

#: holidayregion.cpp:913
msgctxt "HolidayRegion|German region"
msgid "Germany, Saxony-Anhalt"
msgstr "Γερμανία, Σαξωνία-Άνχαλτ"

#: holidayregion.cpp:915
msgctxt "HolidayRegion|German region"
msgid "Germany, Thuringia"
msgstr "Γερμανία, Θουριγγία"

#: holidayregion.cpp:918
msgctxt "HolidayRegion|Spanish region"
msgid "Catalonia"
msgstr "Καταλονία"

#: holidayregion.cpp:920
msgctxt "HolidayRegion|UK Region"
msgid "England and Wales"
msgstr "Αγγλία και Ουαλία"

#: holidayregion.cpp:922
msgctxt "HolidayRegion|UK Region"
msgid "England"
msgstr "Αγγλία"

#: holidayregion.cpp:924
msgctxt "HolidayRegion|UK Region"
msgid "Wales"
msgstr "Ουαλία"

#: holidayregion.cpp:926
msgctxt "HolidayRegion|UK Region"
msgid "Scotland"
msgstr "Σκωτία"

#: holidayregion.cpp:928
msgctxt "HolidayRegion|UK Region"
msgid "Northern Ireland"
msgstr "Βόρεια Ιρλανδία"

#: holidayregion.cpp:930
msgctxt "HolidayRegion|Italian Region"
msgid "South Tyrol"
msgstr "Νότιο Τιρόλο"

#: holidayregion.cpp:932
msgctxt "HolidayRegion|"
msgid "New South Wales"
msgstr "Νέα Νότια Ουαλία"

#: holidayregion.cpp:934
msgctxt "HolidayRegion|Australian Region"
msgid "Queensland"
msgstr "Κουίνσλαντ"

#: holidayregion.cpp:936
msgctxt "HolidayRegion|Australian Region"
msgid "Victoria"
msgstr "Βικτώρια"

#: holidayregion.cpp:938
msgctxt "HolidayRegion|Australian Region"
msgid "South Australia"
msgstr "Νότια Αυστραλία"

#: holidayregion.cpp:940
msgctxt "HolidayRegion|Australian Region"
msgid "Northern Territory"
msgstr "Βόρεια Επικράτεια"

#: holidayregion.cpp:942
msgctxt "HolidayRegion|Australian Region"
msgid "Australian Capital Territory"
msgstr "Επικράτεια Αυστραλιανής Πρωτεύουσας"

#: holidayregion.cpp:944
msgctxt "HolidayRegion|Australian Region"
msgid "Western Australia"
msgstr "Δυτική Αυστραλία"

#: holidayregion.cpp:946
msgctxt "HolidayRegion|Australian Region"
msgid "Tasmania"
msgstr "Τασμανία"

#: holidayregion.cpp:948
msgctxt "HolidayRegion|Bosnian and Herzegovinian Region"
msgid "Republic of Srpska"
msgstr "Σερβική Δημοκρατία"

#: holidayregion.cpp:966
msgctxt "HolidayRegion|Holiday type"
msgid "Public"
msgstr "Δημόσια"

#: holidayregion.cpp:968
msgctxt "HolidayRegion|Holiday type"
msgid "Civil"
msgstr "Πολιτική"

#: holidayregion.cpp:970
msgctxt "HolidayRegion|Holiday type"
msgid "Religious"
msgstr "Θρησκευτική"

#: holidayregion.cpp:972
msgctxt "HolidayRegion|Holiday type"
msgid "Government"
msgstr "Κυβερνητική"

#: holidayregion.cpp:974
msgctxt "HolidayRegion|Holiday type"
msgid "Financial"
msgstr "Χρηματιστηριακή"

#: holidayregion.cpp:976
msgctxt "HolidayRegion|Holiday type"
msgid "Cultural"
msgstr "Πολιτιστική"

#: holidayregion.cpp:978
msgctxt "HolidayRegion|Holiday type"
msgid "Commemorative"
msgstr "Αναμνηστική"

#: holidayregion.cpp:980
msgctxt "HolidayRegion|Holiday type"
msgid "Historical"
msgstr "Ιστορική"

#: holidayregion.cpp:982
msgctxt "HolidayRegion|Holiday type"
msgid "School"
msgstr "Σχολική"

#: holidayregion.cpp:984
msgctxt "HolidayRegion|Holiday type"
msgid "Seasonal"
msgstr "Εποχική"

#: holidayregion.cpp:986
msgctxt "HolidayRegion|Holiday type"
msgid "Name Days"
msgstr "Ονομαστική"

#: holidayregion.cpp:988
msgctxt "HolidayRegion|Holiday type"
msgid "Personal"
msgstr "Προσωπική"

#: holidayregion.cpp:990
msgctxt "HolidayRegion|Holiday type"
msgid "Christian"
msgstr "Χριστιανική"

#: holidayregion.cpp:992
msgctxt "HolidayRegion|Holiday type"
msgid "Anglican"
msgstr "Αγγλικανική"

#: holidayregion.cpp:994
msgctxt "HolidayRegion|Holiday type"
msgid "Catholic"
msgstr "Καθολική"

#: holidayregion.cpp:996
msgctxt "HolidayRegion|Holiday type"
msgid "Protestant"
msgstr "Προτεσταντική"

#: holidayregion.cpp:998
msgctxt "HolidayRegion|Holiday type"
msgid "Orthodox"
msgstr "Ορθόδοξη"

#: holidayregion.cpp:1000
msgctxt "HolidayRegion|Holiday type"
msgid "Jewish"
msgstr "Εβραϊκή"

#: holidayregion.cpp:1002
msgctxt "HolidayRegion|Holiday type"
msgid "Jewish Orthodox"
msgstr "Ebra:ik;h Ορθόδοξη"

#: holidayregion.cpp:1004
msgctxt "HolidayRegion|Holiday type"
msgid "Jewish Conservative"
msgstr "Εβραϊκή συντηρητική"

#: holidayregion.cpp:1006
msgctxt "HolidayRegion|Holiday type"
msgid "Jewish Reform"
msgstr "Εβραϊκή μεταρρυθμιστική"

#: holidayregion.cpp:1008
msgctxt "HolidayRegion|Holiday type"
msgid "Islamic"
msgstr "Ισλαμική"

#: holidayregion.cpp:1010
msgctxt "HolidayRegion|Holiday type"
msgid "Islamic Sunni"
msgstr "Ισλαμική Σούμι"

#: holidayregion.cpp:1012
msgctxt "HolidayRegion|Holiday type"
msgid "Islamic Shia"
msgstr "Ισλαμική Σίιτική"

#: holidayregion.cpp:1014
msgctxt "HolidayRegion|Holiday type"
msgid "Islamic Sufi"
msgstr "Ισλαμική Σούφι"

#: holidayregion.cpp:1020
#, qt-format
msgctxt ""
"HolidayRegion|Holiday file display name, %1 = region name, %2 = holiday type"
msgid "%1 - %2"
msgstr "%1 - %2"

#: holidayregion.cpp:1028
msgctxt "HolidayRegion|Unknown holiday region"
msgid "Unknown"
msgstr "Άγνωστη"

#: lunarphase.cpp:25
msgctxt "LunarPhase|"
msgid "New Moon"
msgstr "Νέα Σελήνη"

#: lunarphase.cpp:27
msgctxt "LunarPhase|"
msgid "Full Moon"
msgstr "Πανσέληνος"

#: lunarphase.cpp:29
msgctxt "LunarPhase|"
msgid "First Quarter Moon"
msgstr "Σελήνη Πρώτο Τέταρτο"

#: lunarphase.cpp:31
msgctxt "LunarPhase|"
msgid "Last Quarter Moon"
msgstr "Σελήνη Τελευταίο Τέταρτο"

#: lunarphase.cpp:35
msgctxt "LunarPhase|"
msgid "Waxing Crescent"
msgstr ""

#: lunarphase.cpp:37
msgctxt "LunarPhase|"
msgid "Waxing Gibbous"
msgstr ""

#: lunarphase.cpp:39
msgctxt "LunarPhase|"
msgid "Waning Gibbous"
msgstr ""

#: lunarphase.cpp:41
msgctxt "LunarPhase|"
msgid "Waning Crescent"
msgstr ""

#: zodiac.cpp:66
msgctxt "Zodiac|"
msgid "Aries"
msgstr "Κριός"

#: zodiac.cpp:68
msgctxt "Zodiac|"
msgid "Taurus"
msgstr "Ταύρος"

#: zodiac.cpp:70
msgctxt "Zodiac|"
msgid "Gemini"
msgstr "Δίδυμοι"

#: zodiac.cpp:72
msgctxt "Zodiac|"
msgid "Cancer"
msgstr "Καρκίνος"

#: zodiac.cpp:74
msgctxt "Zodiac|"
msgid "Leo"
msgstr "Λέων"

#: zodiac.cpp:76
msgctxt "Zodiac|"
msgid "Virgo"
msgstr "Παρθένος"

#: zodiac.cpp:78
msgctxt "Zodiac|"
msgid "Libra"
msgstr "Ζυγός"

#: zodiac.cpp:80
msgctxt "Zodiac|"
msgid "Scorpio"
msgstr "Σκορπιός"

#: zodiac.cpp:82
msgctxt "Zodiac|"
msgid "Sagittarius"
msgstr "Τοξότης"

#: zodiac.cpp:84
msgctxt "Zodiac|"
msgid "Capricorn"
msgstr "Αιγόκερως"

#: zodiac.cpp:86
msgctxt "Zodiac|"
msgid "Aquarius"
msgstr "Υδροχόος"

#: zodiac.cpp:88
msgctxt "Zodiac|"
msgid "Pisces"
msgstr "Ιχθύς"

#: zodiac.cpp:258
msgctxt "HolidayRegion|zodiac symbol for Aries"
msgid "ram"
msgstr "κριός"

#: zodiac.cpp:260
msgctxt "HolidayRegion|zodiac symbol for Taurus"
msgid "bull"
msgstr "ταύρος"

#: zodiac.cpp:262
msgctxt "HolidayRegion|zodiac symbol for Gemini"
msgid "twins"
msgstr "δίδυμοι"

#: zodiac.cpp:264
msgctxt "HolidayRegion|zodiac symbol for Cancer"
msgid "crab"
msgstr "καβούρι"

#: zodiac.cpp:266
msgctxt "HolidayRegion|zodiac symbol for Leo"
msgid "lion"
msgstr "λιοντάρι"

#: zodiac.cpp:268
msgctxt "HolidayRegion|zodiac symbol for Virgo"
msgid "virgin"
msgstr "παρθένος"

#: zodiac.cpp:270
msgctxt "HolidayRegion|zodiac symbol for Libra"
msgid "scales"
msgstr "ζυγαριά"

#: zodiac.cpp:272
msgctxt "HolidayRegion|zodiac symbol for Scorpion"
msgid "scorpion"
msgstr "σκορπιός"

#: zodiac.cpp:274
msgctxt "HolidayRegion|zodiac symbol for Sagittarius"
msgid "archer"
msgstr "τοξότης"

#: zodiac.cpp:276
msgctxt "HolidayRegion|zodiac symbol for Capricorn"
msgid "goat"
msgstr "αίγα"

#: zodiac.cpp:278
msgctxt "HolidayRegion|zodiac symbol for Aquarius"
msgid "water carrier"
msgstr "υδροχόος"

#: zodiac.cpp:280
msgctxt "HolidayRegion|zodiac symbol for Pices"
msgid "fish"
msgstr "ιχθύς"

#~ msgid "<p>Select to use Holiday Region</p>"
#~ msgstr "<p>Επιλέξτε χρήση περιοχής αργιών</p>"

#~ msgid ""
#~ "<p><b>Region:</b> %1<br/><b>Language:</b> %2<br/><b>Description:</b> %3</"
#~ "p>"
#~ msgstr ""
#~ "<p><b>Περιοχή:</b> %1<br/><b>Γλώσσα:</b> %2<br/><b>Περιγραφή:</b> %3</p>"

#~ msgid ""
#~ "<p>You can choose to display the Holiday Region for information only, or "
#~ "to use the Holiday Region when displaying or calculating days off such as "
#~ "Public Holidays.  If you choose to use the Holiday Region for Days Off, "
#~ "then only those Holiday Events marked in the Holiday Region as Days Off "
#~ "will be used for non-work days, Holiday Events that are not marked in the "
#~ "Holiday Region as Days Off will continue to be work days.</p>"
#~ msgstr ""
#~ "<p>Μπορείτε να επιλέξετε να εμφανίζεται η περιοχή αργιών μόνο για "
#~ "ενημέρωση, ή να χρησιμοποιήσετε την περιοχή αργιών όταν εμφανίζονται ή "
#~ "υπολογίζονται οι εκτός εργασίας ημέρες όπως οι δημόσιες αργίες. Αν "
#~ "επιλέξετε στη χρήση της περιοχής αργιών τις μη εργάσιμες ημέρες, τότε "
#~ "μόνο οι αργίες που έχουν σημειωθεί στην περιοχή αργιών ως μη εργάσιμες θα "
#~ "χρησιμοποιηθούν ως τέτοιες, αργίες που δεν έχουν σημειωθεί στην περιοχή "
#~ "αργιών ως μη εργάσιμες θα θεωρούνται εργάσιμες.</p>"

#~ msgctxt "Combobox label, Holiday Region not used"
#~ msgid "Not Used"
#~ msgstr "Δεν χρησιμοποιείται"

#~ msgctxt "Combobox label, use Holiday Region for information only"
#~ msgid "Information"
#~ msgstr "Ενημερωτικά"

#~ msgctxt "Combobox label, use Holiday Region for days off"
#~ msgid "Days Off"
#~ msgstr "Αργίες"

#~ msgctxt "Header for Select column"
#~ msgid "Select"
#~ msgstr "Επιλογή"

#~ msgid "<p>This column selects to use the Holiday Region</p>"
#~ msgstr "<p>Αυτή η στήλη επιλέγει τη χρήση της περιοχής αργιών</p>"

#~ msgid "<p>This column displays the name of the Holiday Region</p>"
#~ msgstr "<p>Αυτή η στήλη εμφανίζει το όνομα της περιοχής αργιών</p>"

#~ msgctxt "Header for Language column"
#~ msgid "Language"
#~ msgstr "Γλώσσα"

#~ msgid "<p>This column displays the language of the Holiday Region</p>"
#~ msgstr "<p>Η στήλη αυτή εμφανίζει τη γλώσσα της περιοχής αργιών</p>"

#~ msgid "<p>This column displays the description of the Holiday Region</p>"
#~ msgstr "<p>Η στήλη αυτή εμφανίζει την περιγραφή της περιοχής αργιών</p>"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Τούσης Μανώλης"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "manolis@koppermind.homelinux.org"

#~ msgctxt "zodiac symbol for Aquarius"
#~ msgid "water"
#~ msgstr "υδροχόος"
